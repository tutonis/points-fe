# points-fe

This project is generated using `vue-cli` currently using:

  * babel - transpiler
  * eslint - standard rules set
  * vue-router - spa navigation
  * vuex - for global state management (kind of redux alternative)
  * vuetify - ui elements framework
  * axios - universal http client
  * lodash - general helper library
  * worker-plugin - simplifies bundling wer worker resources
  
Heavy computational tasks are being done in front end:
  * file import - uses `file` api
  * square calculation - leverages `webworkers` to offload computation from main thread
  
Square calculation could have been achieved doing calculations in back end and leveraging `Server-Sent Events`
to achieve continuous stream af data.

Since this is very cool spa I thought that it would be suitable to have more interesting functionality
like saving user data without persisting it to backend in between eg. navigations (local save button).
As I also found out this also adds more complexity. Taming it was still fun exercise,
but surely there is some space for future simplifications (and more test coverage) 

Another point of simplification would be dividing main UI features to different views/routes.

```
Please do not judge of component communication by last (today's) commits.
Collections store selected projection was bolt on too late in app development (this morning)...
Streamlining it would actually be the very first task in todo list. This would greatly
simplify code and remove some events communication. 
```


# Future app development

0. more tests coverage
1. adopt gitflow or other branch management strategy
2. add gitlab config for ci/cd - leveraging `aws` free tier for hosting
4. convert app to `pwa` - basic app shell pattern would be a good start
5. optimise bundle size/splitting strategies
5. Add translations using js library


# launching app
To launch local server (port *8080*): 
```
* checkout repo
* npm install
* npm run serve
```
