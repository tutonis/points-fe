export default {
  promisifySuccess: data => {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(data), 100)
    })
  },
  promisifyFailure: data => {
    return new Promise((resolve, reject) => {
      reject(new Error('This is baaad, very bad'))
    })
  }
}
