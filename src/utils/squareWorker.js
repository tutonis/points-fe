addEventListener('message', e => {
  calculateSquares(e.data)
})

const calculateSquares = points => {
  const allPossibleLines = [].concat(
    ...points.map((v, i) => points.slice(i + 1).map(w => [v, w]))
  )

  const groupByLength = (final, { 0: p1, 1: p2 }) => {
    const veryAprox = n => Math.round(n * 10) / 10

    const length = veryAprox(Math.hypot(p2.x - p1.x, p2.y - p1.y))

    if (!final[length]) {
      final[length] = []
    }

    final[length].push([p1, p2])

    return final
  }

  const byLength = allPossibleLines.reduce(groupByLength, {})

  const toSquare = l => ({
    p1: `${l[0][0].x}:${l[0][0].y}`,
    p2: `${l[0][1].x}:${l[0][1].y}`,
    p3: `${l[1][0].x}:${l[1][0].y}`,
    p4: `${l[1][1].x}:${l[1][1].y}`
  })

  const vals = Object.values(byLength).filter(v => v.length > 2)

  const send = i => {
    setTimeout(() => {
      postMessage(toSquare(vals[i]))
    }, i * 300)
  }

  for (let i = 0; i < vals.length; i++) {
    (send)(i)
  }
}
