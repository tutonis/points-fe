import { points } from '@/utils/constants/validation'

export const validateAndMap = data => {
  const structure = { data: {}, errors: [], warnings: [] }

  const { data: dataObj, errors, warnings } = data.reduce(doTheWorkBby, structure)

  return {
    data: Object.values(dataObj),
    errors,
    warnings
  }
}

const doTheWorkBby = (obj, lineData, index, orig) => {
  const line = index + 1

  const addError = message => (obj.errors.push({ line, message }))
  const addWarning = message => (obj.warnings.push({ line, message }))

  const isListFull = o => Object.keys(o.data).length === points.maxItems

  if (isListFull(obj)) return obj

  // not empty
  if (lineData.length === 0) return obj

  // exactly 2 elements before parsing
  if (lineData.length !== 2) {
    addError('Wrong point data - not 2 items')
    return obj
  }

  // do the parsing and add validation messages
  const parsed = lineData
    .map(d => {
      const int = Number.parseFloat(d)

      // is an integer
      if (!Number.isInteger(int)) {
        addError('Not an integer')
        return null
      }

      if (int < points.minVal) {
        addError(`Min value must be >= ${points.minVal}`)
        return null
      }

      if (int > points.maxVal) {
        addError(`Max value must be <= ${points.maxVal}`)
        return null
      }

      return int
    })
    .filter(x => x)

  // after parsing there still needs to be 2 elements
  if (parsed.length !== 2) return obj

  // uniqueness and actually adding
  const key = parsed.toString()

  if (!obj.data[key]) {
    const [x, y] = parsed
    obj.data[key] = { x, y }
  } else {
    addError('Duplicate values')
  }

  const isThereNextItem = orig.length > index + 1
  if (isListFull(obj) && isThereNextItem) {
    addWarning(`We've reached the end of the line. It is ${points.maxItems} points max`)
  }

  return obj
}
