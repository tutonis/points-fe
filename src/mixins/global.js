export default {
  methods: {
    addError (message) {
      this.$store.commit('system/addError', message)
    },
    clearErrors () {
      this.$store.commit('system/clearErrors')
    },
    addWarning (message) {
      this.$store.commit('system/addWarning', message)
    },
    clearWarnings () {
      this.$store.commit('system/clearWarnings')
    },
    addInfo (message) {
      this.$store.commit('system/addInfo', message)
    },
    clearInfos () {
      this.$store.commit('system/clearInfos')
    },
    clearAllMessages () {
      this.$store.commit('system/clearAllMessages')
    }
  },
  beforeRouteEnter (to, from, next) {
    next(vm => (vm.clearAllMessages()))
  }
}
