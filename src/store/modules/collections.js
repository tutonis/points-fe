import Vue from 'vue'

const state = {
  collections: {},
  selected: null
}

const actions = {}

const getters = {
  getAllCollections: s => {
    return Object.values(s.collections)
  },
  getCollection: s => id => {
    return s.collections[id]
  }
}

const mutations = {
  saveCollection (s, { id, collection }) {
    Vue.set(s.collections, id, collection)
  },
  deleteCollection (s, id) {
    Vue.delete(s.collections, id)
  },
  selectCollection (s, collection) {
    s.selected = collection
  },
  deselectCollection (s) {
    s.selected = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
