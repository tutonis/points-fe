import Vue from 'vue'

const state = {
  points: {}
}

const actions = {}

const getters = {
  getPoints: s => id => {
    return s.points[id]
  }
}

const mutations = {
  savePoints (s, { points, collectionId }) {
    Vue.set(s.points, collectionId, points)
  },
  deletePoints (s, collectionId) {
    Vue.delete(s.points, collectionId)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
