const state = {
  errors: [],
  warnings: [],
  infos: []
}

const getters = {}
const actions = {}

const mutations = {
  addError (s, payload) {
    if (Array.isArray(payload)) s.errors.push(...payload)
    else s.errors.push(payload)
  },
  clearErrors (s) {
    s.errors = []
  },
  addWarning (s, payload) {
    if (Array.isArray(payload)) s.warnings.push(...payload)
    else s.warnings.push(payload)
  },
  clearWarnings (s) {
    s.warnings = []
  },
  addInfo (s, payload) {
    if (Array.isArray(payload)) s.infos.push(...payload)
    else s.infos.push(payload)
  },
  clearInfos (s) {
    s.infos = []
  },
  clearAllMessages (s) {
    s.errors = []
    s.warnings = []
    s.infos = []
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
