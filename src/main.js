import Vue from 'vue'
import App from '@/App.vue'
import router from '@/plugins/router'
import store from '@/plugins/store'
import vuetify from '@/plugins/vuetify'
import global from '@/mixins/global'
import components from '@/components'

Vue.config.productionTip = false

Vue.mixin(global)

components.register()

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
