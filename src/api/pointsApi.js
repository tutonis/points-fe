import http from '@/api/http'

export default {
  fetchByCollection: id => {
    // const data = [
    //   { id: 1, x: 0, y: 0 },
    //   { id: 2, x: 4, y: 6 },
    //   { id: 3, x: 8, y: 1 },
    //   { id: 4, x: 5, y: 14 },
    //   { id: 5, x: 22, y: 16 },
    //   { id: 6, x: 11, y: 4 },
    //   { id: 7, x: 9, y: 99 },
    //   { id: 8, x: 12, y: 12 },
    //   { id: 9, x: 1, y: 8 },
    //   { id: 10, x: 5, y: 22 },
    //   { id: 11, x: 5, y: 6 },
    //   { id: 12, x: 5, y: 26 },
    //   { id: 13, x: 10, y: 14 },
    //   { id: 14, x: 7, y: 5 },
    //   { id: 15, x: 5, y: 31 },
    //   { id: 16, x: 7, y: 24 }
    // ]

    return http.get(`/collections/${id}/points/`)
  }
}
