import http from '@/api/http'

export default {
  fetchAll: () => {
    return http.get('/collections/')
  },
  deleteCollection: id => {
    return http.delete(`/collections/${id}/`)
  },
  renameCollection: (id, name) => {
    return http.post(`/collections/${id}/rename`, {
      name
    })
  },
  createCollection: ({ name, points }) => {
    return http.post(`/collections/`, {
      name,
      points: points || []
    })
  },
  updateCollection: (id, { name, points }) => {
    return http.put(`/collections/${id}/`, {
      name,
      points: points || []
    })
  }
}
