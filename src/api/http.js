import axios from 'axios'

const errorHandler = error => {
  // console.log('We got http problem huston!', error.message)
  return Promise.reject(new Error(error))
}

const successHandler = response => response

const instance = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 6000
})

instance.interceptors.response.use(
  response => successHandler(response),
  error => errorHandler(error)
)

export default instance
