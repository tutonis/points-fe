import Vue from 'vue'
import Vuex from 'vuex'
import system from '@/store/modules/system'
import collections from '@/store/modules/collections'
import points from '@/store/modules/points'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    system,
    points,
    collections
  },
  strict: process.env.NODE_ENV !== 'production'
})
