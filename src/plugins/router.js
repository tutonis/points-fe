import Vue from 'vue'
import VueRouter from 'vue-router'
import PointsView from '@/views/PointsView'
import HelloView from '@/views/HelloView'
import SquaresView from '@/views/SquaresView'

Vue.use(VueRouter)

const dynamicProps = route => Object.assign({}, route.params, route.query)

const routes = [
  { path: '/', component: PointsView },
  { path: '/hello', component: HelloView },
  { path: '/squares', component: SquaresView, props: dynamicProps }
]

export default new VueRouter({
  mode: 'history',
  routes
})
