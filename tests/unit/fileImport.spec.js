import { validateAndMap } from '@/utils/fileUtils'

describe('File import validation and mapping test', () => {
  let data = []

  beforeAll(() => {
    data = [
      [16, 10],
      [10, 19],
      [8, 14, 111], // 1
      [3, 9],
      [20, 16],
      [6, 10, 112], // 2
      [15, 3],
      [1, 12],
      [3, 1],
      [1, 12], // 3
      [-1, 1],
      [15, 5],
      [-1, 1], // 4
      [12, 0],
      [5001, 19], // 5
      [8, 5001], // 6
      [-5001, 19], // 7
      [1, -5001], // 8
      [9, 16],
      [6, 3],
      [11, 5],
      [18, 10.22], // 9
      [3, 17]
    ]
  })

  it('should detect errors count', () => {
    const { errors } = validateAndMap(data)

    expect(errors.length).toBe(9)
  })
})
