import { createLocalVue, shallowMount } from '@vue/test-utils'
import Vuetify from 'vuetify'
import CollectionControls from '@/components/points/collection/CollectionControls'

describe('PointsCollection.vue', () => {
  let localVue

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify)
  })

  it('should emit event', () => {
    const controls = shallowMount(CollectionControls, { localVue })

    controls.vm.$emit('rename')

    expect(controls.emitted()).toBeTruthy()
  })
})
